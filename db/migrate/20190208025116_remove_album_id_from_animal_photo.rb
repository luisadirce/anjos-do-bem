class RemoveAlbumIdFromAnimalPhoto < ActiveRecord::Migration[5.2]
  def change
    remove_column :animal_photos, :album_id
  end
end
