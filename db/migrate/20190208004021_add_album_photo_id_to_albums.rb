class AddAlbumPhotoIdToAlbums < ActiveRecord::Migration[5.2]
  def change
    add_reference :albums, :album_photo, foreign_key: true
    add_reference :album_photos, :album, foreign_key: true
  end
end
