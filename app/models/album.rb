class Album < ApplicationRecord
    has_many :album_photos, dependent: :destroy

    validates :name, presence: true, length: { maximum: 100 }

    mount_uploader :cover_photo, PhotoUploader
end
