module ApplicationHelper
    def is_admin?
        current_admin.present?
    end
end
